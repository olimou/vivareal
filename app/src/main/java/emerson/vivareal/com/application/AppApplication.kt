package emerson.vivareal.com.application

import android.app.Application
import emerson.vivareal.com.application.di.AppComponent
import emerson.vivareal.com.application.di.AppModule
import emerson.vivareal.com.application.di.DaggerAppComponent
import emerson.vivareal.com.view.games.details.di.GameDetailComponent
import emerson.vivareal.com.view.games.details.di.GameDetailModule
import emerson.vivareal.com.view.games.list.bookmark.di.BookmarkListComponent
import emerson.vivareal.com.view.games.list.bookmark.di.BookmarkListModule
import emerson.vivareal.com.view.games.list.games.di.GamesListComponent
import emerson.vivareal.com.view.games.list.games.di.GamesListModule

class AppApplication : Application() {

    lateinit var appComponent: AppComponent
    lateinit var gamesListComponent: GamesListComponent
    lateinit var gameDetailComponent: GameDetailComponent
    lateinit var bookmarkListComponent: BookmarkListComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()

        gamesListComponent = appComponent.inject(GamesListModule())
        gameDetailComponent = appComponent.inject(GameDetailModule())
        bookmarkListComponent = appComponent.inject(BookmarkListModule())
    }
}