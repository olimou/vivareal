package emerson.vivareal.com.application.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import emerson.vivareal.com.R
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(@Named("baseUrl") baseUrl: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build()
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor,
                            @Named("headerParameters") header: Interceptor,
                            cacheDir: Cache): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(header)
                .addInterceptor(loggingInterceptor)
                .cache(cacheDir)
                .build()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Singleton
    @Provides
    @Named("headerParameters")
    fun provideHttpHeaderInterceptor(context: Context): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()

            val apiKey: String = context.getString(R.string.twitch_api)

            val requestBuilder = original?.newBuilder()
                    ?.addHeader("Client-ID", apiKey)
                    ?.method(original.method(), original.body())
                    ?.url(original.url())

            val request = requestBuilder?.build()

            chain.proceed(request)
        }
    }

    @Singleton
    @Provides
    fun provideHttpClientCache(cacheDir: File): Cache {
        val cacheSize = 10 * 1024 * 1024.toLong()

        val cache = Cache(cacheDir, cacheSize)

        return cache
    }
}