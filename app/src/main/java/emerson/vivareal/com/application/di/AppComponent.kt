package emerson.vivareal.com.application.di

import dagger.Component
import emerson.vivareal.com.view.games.details.di.GameDetailComponent
import emerson.vivareal.com.view.games.details.di.GameDetailModule
import emerson.vivareal.com.view.games.list.bookmark.di.BookmarkListComponent
import emerson.vivareal.com.view.games.list.bookmark.di.BookmarkListModule
import emerson.vivareal.com.view.games.list.games.di.GamesListComponent
import emerson.vivareal.com.view.games.list.games.di.GamesListModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, ApiModule::class])
interface AppComponent {
    fun inject(listModule: GamesListModule): GamesListComponent
    fun inject(detailModule: GameDetailModule): GameDetailComponent
    fun inject(bookmarkListModule: BookmarkListModule): BookmarkListComponent
}