package emerson.vivareal.com.application.di

import dagger.Module
import dagger.Provides
import emerson.vivareal.com.data.api.TopGames.TopGamesAPI
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return "https://api.twitch.tv/kraken/games/"
    }

    @Provides
    @Singleton
    fun provideTopGamesApi(retrofit: Retrofit): TopGamesAPI {
        return retrofit.create(TopGamesAPI::class.java)
    }
}