package emerson.vivareal.com.application

import android.databinding.BindingAdapter
import android.support.design.widget.FloatingActionButton
import com.airbnb.lottie.LottieAnimationView
import emerson.vivareal.com.view.custom.frescoimage.ImageResUrls
import emerson.vivareal.com.view.custom.frescoimage.LoadImageListener
import emerson.vivareal.com.view.custom.frescoimage.UrlImage

@BindingAdapter("imageUrl")
fun setImageUrl(imageView: UrlImage, url: ImageResUrls) {
    imageView.setImageRes(url)
}

@BindingAdapter("onLoadFinish")
fun setOnLoadFinishListener(frescoImage: UrlImage, listener: LoadImageListener) {
    frescoImage.listener = listener
}

@BindingAdapter("backgroundTint")
fun setBackgroundTint(floatingActionButton: FloatingActionButton, colorRes: Int) {
    floatingActionButton.backgroundTintList = floatingActionButton.context.resources.getColorStateList(colorRes)
}

@BindingAdapter("lottie_rawRes")
fun setAnimation(lottieAnimationView: LottieAnimationView, animationRes: Int?) {
    animationRes?.let {
        lottieAnimationView.setAnimation(it)
    }
}
