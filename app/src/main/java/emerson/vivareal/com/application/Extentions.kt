package emerson.vivareal.com.application

import android.app.Activity
import android.support.v4.app.Fragment

val Fragment.appApplication: AppApplication
    get() {
        return this.activity?.application as AppApplication
    }

val Activity.appApplication: AppApplication
    get() {
        return this.application as AppApplication
    }