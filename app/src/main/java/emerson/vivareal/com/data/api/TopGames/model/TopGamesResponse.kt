package emerson.vivareal.com.data.api.TopGames.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class TopGamesResponse(@SerializedName("top")
                            val top: List<TopItem>?,
                            @SerializedName("_links")
                            val Links: Links,
                            @SerializedName("_total")
                            val Total: Int = 0) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(TopItem),
            parcel.readParcelable(emerson.vivareal.com.data.api.TopGames.model.Links::class.java.classLoader),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(top)
        parcel.writeParcelable(Links, flags)
        parcel.writeInt(Total)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TopGamesResponse> {
        override fun createFromParcel(parcel: Parcel): TopGamesResponse {
            return TopGamesResponse(parcel)
        }

        override fun newArray(size: Int): Array<TopGamesResponse?> {
            return arrayOfNulls(size)
        }
    }
}