package emerson.vivareal.com.data.api.TopGames

import emerson.vivareal.com.data.api.TopGames.model.TopGamesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface TopGamesAPI {

    @GET("top")
    fun getTop(@Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<TopGamesResponse>
}