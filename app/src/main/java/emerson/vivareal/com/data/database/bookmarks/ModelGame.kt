package emerson.vivareal.com.data.database.bookmarks

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import emerson.vivareal.com.data.api.TopGames.model.TopItem

@Entity
class ModelGame() : Parcelable {

    @PrimaryKey
    var id: Int = 0

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "viewers")
    var viewers: Int = 0

    @ColumnInfo(name = "box_small")
    var box_small: String? = null

    @ColumnInfo(name = "box_larger")
    var box_larger: String? = null

    @ColumnInfo(name = "logo")
    var logo: String? = null

    var isBookmark = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as Int
        name = parcel.readString()
        viewers = parcel.readInt()
        box_small = parcel.readString()
        box_larger = parcel.readString()
        logo = parcel.readString()
        isBookmark = parcel.readByte() != 0.toByte()
    }

    constructor(topItem: TopItem) : this() {
        this.id = topItem.game.Id
        this.name = topItem.game.name
        this.viewers = topItem.viewers
        this.box_small = topItem.game.box.small
        this.box_larger = topItem.game.box.large
        this.logo = topItem.game.logo.large
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeInt(viewers)
        parcel.writeString(box_small)
        parcel.writeString(box_larger)
        parcel.writeString(logo)
        parcel.writeByte(if (isBookmark) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ModelGame

        if (id != other.id) return false
        if (name != other.name) return false
        if (viewers != other.viewers) return false
        if (box_small != other.box_small) return false
        if (box_larger != other.box_larger) return false
        if (logo != other.logo) return false
        if (isBookmark != other.isBookmark) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + viewers
        result = 31 * result + (box_small?.hashCode() ?: 0)
        result = 31 * result + (box_larger?.hashCode() ?: 0)
        result = 31 * result + (logo?.hashCode() ?: 0)
        result = 31 * result + isBookmark.hashCode()
        return result
    }

    companion object CREATOR : Parcelable.Creator<ModelGame> {
        override fun createFromParcel(parcel: Parcel): ModelGame {
            return ModelGame(parcel)
        }

        override fun newArray(size: Int): Array<ModelGame?> {
            return arrayOfNulls(size)
        }
    }
}