package emerson.vivareal.com.data.api.TopGames.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Game(@SerializedName("_links")
                val Links: Links,
                @SerializedName("giantbomb_id")
                val giantbombId: Int = 0,
                @SerializedName("popularity")
                val popularity: Int = 0,
                @SerializedName("name")
                val name: String = "",
                @SerializedName("logo")
                val logo: Logo,
                @SerializedName("box")
                val box: Box,
                @SerializedName("_id")
                val Id: Int = 0,
                @SerializedName("locale")
                val locale: String = "",
                @SerializedName("localized_name")
                val localizedName: String = "") : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(emerson.vivareal.com.data.api.TopGames.model.Links::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readParcelable(Logo::class.java.classLoader),
            parcel.readParcelable(Box::class.java.classLoader),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(Links, flags)
        parcel.writeInt(giantbombId)
        parcel.writeInt(popularity)
        parcel.writeString(name)
        parcel.writeParcelable(logo, flags)
        parcel.writeParcelable(box, flags)
        parcel.writeInt(Id)
        parcel.writeString(locale)
        parcel.writeString(localizedName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Game> {
        override fun createFromParcel(parcel: Parcel): Game {
            return Game(parcel)
        }

        override fun newArray(size: Int): Array<Game?> {
            return arrayOfNulls(size)
        }
    }
}