package emerson.vivareal.com.data.api.TopGames.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class TopItem(@SerializedName("game")
                   val game: Game,
                   @SerializedName("viewers")
                   val viewers: Int = 0,
                   @SerializedName("channels")
                   val channels: Int = 0) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Game::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(game, flags)
        parcel.writeInt(viewers)
        parcel.writeInt(channels)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TopItem> {
        override fun createFromParcel(parcel: Parcel): TopItem {
            return TopItem(parcel)
        }

        override fun newArray(size: Int): Array<TopItem?> {
            return arrayOfNulls(size)
        }
    }
}