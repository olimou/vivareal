package emerson.vivareal.com.data.database.bookmarks

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface BookmarkDao {
    @Query("SELECT * FROM ModelGame")
    fun getAll(): Flowable<List<ModelGame>>

    @Query("SELECT * FROM ModelGame WHERE id LIKE :gameId")
    fun loadById(gameId: Int): Flowable<ModelGame>

    @Insert
    fun insert(modelGame: ModelGame)

    @Delete
    fun delete(modelGame: ModelGame)
}