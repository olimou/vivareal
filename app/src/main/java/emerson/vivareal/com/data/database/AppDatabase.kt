package emerson.vivareal.com.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import emerson.vivareal.com.data.database.bookmarks.BookmarkDao
import emerson.vivareal.com.data.database.bookmarks.ModelGame

@Database(entities = [ModelGame::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun bookmarkDao(): BookmarkDao
}