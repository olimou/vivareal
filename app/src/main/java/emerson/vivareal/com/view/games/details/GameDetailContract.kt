package emerson.vivareal.com.view.games.details

import emerson.vivareal.com.data.database.bookmarks.ModelGame

interface GameDetailContract {
    interface View {
        fun updateBookmark(isBookmark:Boolean)
    }

    interface Presenter {
        fun pause()
        fun setDetailView(view: GameDetailContract.View)
        fun bookmark(modelGame: ModelGame)
    }
}