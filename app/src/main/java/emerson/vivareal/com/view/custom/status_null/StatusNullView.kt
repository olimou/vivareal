package emerson.vivareal.com.view.custom.status_null

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import emerson.vivareal.com.databinding.StatusNullItemsBinding
import kotlinx.android.synthetic.main.status_null_items.view.*

class StatusNullView : FrameLayout, StatusNullContract.Presenter {
    var binding: StatusNullItemsBinding? = null

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    fun init() {
        binding = StatusNullItemsBinding.inflate(LayoutInflater.from(context), this, true)

        invalidate()

        img_lottie.playAnimation()
    }

    override fun setObservable(statusNullContract: StatusNullContract.View) {
        binding?.observable = statusNullContract
    }
}