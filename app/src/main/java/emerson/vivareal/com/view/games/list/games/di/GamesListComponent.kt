package emerson.vivareal.com.view.games.list.games.di

import dagger.Subcomponent
import emerson.vivareal.com.view.games.list.games.GamesListFragment

@Subcomponent(modules = [GamesListModule::class])
interface GamesListComponent {
    fun inject(gamesFragment: GamesListFragment)
}