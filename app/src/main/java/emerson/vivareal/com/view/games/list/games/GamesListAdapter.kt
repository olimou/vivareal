package emerson.vivareal.com.view.games.list.games

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.databinding.AdapterGameItemGridBinding
import emerson.vivareal.com.databinding.AdapterGameItemListBinding
import emerson.vivareal.com.view.games.list.holder.GameItemGridHolder
import emerson.vivareal.com.view.games.list.holder.GameItemHolder
import emerson.vivareal.com.view.games.list.holder.GameItemListHolder

class GamesListAdapter :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = GamesListAdapter::class.java.simpleName
    private var listOriginal: MutableSet<ModelGame> = mutableSetOf()
    private var listView: MutableSet<ModelGame> = mutableSetOf()
    private var listDatabaseBookmark: MutableSet<ModelGame> = mutableSetOf()
    private var loading = false

    var presenter: GamesListContract.Presenter? = null
    var view: GamesListContract.View? = null
    var gridLayout = true
    var autoLoad = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return if (gridLayout) {
            GameItemGridHolder(AdapterGameItemGridBinding.inflate(layoutInflater, parent, false))
        } else {
            GameItemListHolder(AdapterGameItemListBinding.inflate(layoutInflater, parent, false))
        }
    }

    override fun getItemCount(): Int {
        val size = listView.size

        view?.statusNullConnection(size == 0 && !loading)

        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (autoLoad && !loading && itemCount > 0 && position > (itemCount - 3)) {
            loading = true
            presenter?.loadMore()
        }

        val model = listView.elementAt(position)

        val gameItemHolder = holder as GameItemHolder

        gameItemHolder.setup(model)

        gameItemHolder.toggleBookmark {
            presenter?.toggleBookmark(model)
        }

        gameItemHolder.openDetails { modelGame, views ->
            view?.openDetails(modelGame, views)
        }
    }

    fun addItems(list: List<ModelGame>) {
        val size = listView.size

        listView.addAll(list)

        listOriginal.addAll(list)

        updateBookmark()

        notifyItemRangeInserted(size, list.size)

        loading = false
    }

    fun addBookmark(id: Int?) {
        listView.find { it.id == id }?.let {
            if (!it.isBookmark) {
                it.isBookmark = true

                notifyItemChanged(listView.indexOf(it))

                updateBookmark()
            }
        }
    }

    fun removeBookmark(id: Int?) {
        val modelGame = listView.find { it.id == id }?.let {
            if (it.isBookmark) {
                it.isBookmark = false

                notifyItemChanged(listView.indexOf(it))

                updateBookmark()
            }
        }
    }

    private fun updateBookmark() {
        listDatabaseBookmark.forEach { database ->
            listView.find { it.id == database.id }?.isBookmark = true
        }
    }

    fun clear() {
        listView.clear()

        notifyDataSetChanged()
    }

    fun filter(stringExtra: String) {
        listView = listOriginal

        updateBookmark()

        if (stringExtra.isNotEmpty()) {
            autoLoad = false

            val query = stringExtra.toLowerCase()

            val filter: List<ModelGame> = listView.filter {
                it.name?.toLowerCase()?.contains(query) ?: false
            }

            listView = filter.toMutableSet()
        } else {
            autoLoad = true
        }

        notifyDataSetChanged()
    }
}