package emerson.vivareal.com.view.games.list.holder

import android.animation.ValueAnimator
import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v7.graphics.Palette
import android.view.View
import emerson.vivareal.com.BR
import emerson.vivareal.com.R
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.view.custom.frescoimage.ImageResUrls

data class GameItemModel(
        private val context: Context,
        val model: ModelGame) : BaseObservable() {

    private val TAG = GameItemModel::class.java.simpleName

    var background = Color.WHITE
        @Bindable get

    var titleColor = Color.DKGRAY
        @Bindable get

    var textColor = Color.DKGRAY
        @Bindable get

    var bookMarkListener: View.OnClickListener? = null

    var bookmarkColor: Int = Color.DKGRAY
        @Bindable get() {
            return if (isBookmark) {
                context.resources.getColor(R.color.colorAccent
                )
            } else {
                context.resources.getColor(R.color.md_grey_600)
            }
        }

    var bookmarkIcon: Drawable? = null
        get() {
            return if (isBookmark) {
                context.resources.getDrawable(R.drawable.ic_bookmark_black_24dp)
            } else {
                context.resources.getDrawable(R.drawable.ic_bookmark_border_black_24dp)
            }
        }

    var buttonTextColor: Int = Color.DKGRAY
        @Bindable get

    var isBookmark: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.bookmarkColor)
        }

    fun getNome(): String? {
        return model.name
    }

    fun getBox(): ImageResUrls {
        return ImageResUrls(model.box_larger)
    }

    fun imageLoadFinish(bitmap: Bitmap) {
        Palette.from(bitmap).generate {
            it.darkVibrantSwatch?.let {
                it.rgb.let {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        val animator = ValueAnimator.ofArgb(Color.WHITE, it)
                        animator.duration = 200
                        animator.addUpdateListener { animation ->
                            background = animation.animatedValue as Int
                            notifyPropertyChanged(BR.background)
                        }
                        animator.start()
                    } else {
                        background = it
                        notifyPropertyChanged(BR.background)
                    }

                    buttonTextColor = it
                    notifyPropertyChanged(BR.buttonTextColor)
                }

                it.titleTextColor.let {
                    textColor = it
                    notifyPropertyChanged(BR.textColor)
                }

                it.bodyTextColor.let {
                    titleColor = it
                    notifyPropertyChanged(BR.titleColor)
                }
            }
        }
    }

    fun getViews(): String {
        return "${model.viewers} ${context.getString(R.string.views)}"
    }
}