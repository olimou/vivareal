package emerson.vivareal.com.view.games.list

import emerson.vivareal.com.data.api.TopGames.TopGamesAPI
import emerson.vivareal.com.data.api.TopGames.model.TopGamesResponse
import io.reactivex.Observable

class GamesListService(private val api: TopGamesAPI) {

    fun loadTopGames(offset: Int, limit: Int): Observable<TopGamesResponse> {
        return api.getTop(offset, limit)
    }
}