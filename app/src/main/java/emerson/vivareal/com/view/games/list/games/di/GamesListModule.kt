package emerson.vivareal.com.view.games.list.games.di

import dagger.Module
import dagger.Provides
import emerson.vivareal.com.data.api.TopGames.TopGamesAPI
import emerson.vivareal.com.data.database.AppDatabase
import emerson.vivareal.com.view.games.list.GamesListService
import emerson.vivareal.com.view.games.list.games.GamesListContract
import emerson.vivareal.com.view.games.list.games.GamesListPresenter

@Module
class GamesListModule {

    @Provides
    fun provideGamesAPI(api: TopGamesAPI): GamesListService {
        return GamesListService(api)
    }

    @Provides
    fun provideGamesPresenter(service: GamesListService, appDatabase: AppDatabase): GamesListContract.Presenter {
        return GamesListPresenter(service, appDatabase.bookmarkDao())
    }
}