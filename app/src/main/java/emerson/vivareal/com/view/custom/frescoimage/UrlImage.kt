package emerson.vivareal.com.view.custom.frescoimage

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.util.AttributeSet
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class UrlImage : ImageView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    private val TAG = UrlImage::class.java.simpleName

    var listener: LoadImageListener? = null
        set(value) {
            field = value

            bitmap?.let {
                field?.onFinish(it)
            }
        }

    var bitmap: Bitmap? = null

    fun setImageRes(imageRes: ImageResUrls) {
        Picasso.get().load(imageRes.url).into(this, object : Callback {
                    override fun onSuccess() {
                        val bitmapUrl = (drawable as BitmapDrawable).bitmap

                        this@UrlImage.bitmap = bitmapUrl
                        listener?.onFinish(bitmapUrl)
                    }

                    override fun onError(e: Exception?) {

                    }
                })
    }
}