package emerson.vivareal.com.view.games.list.bookmark

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import emerson.vivareal.com.R
import emerson.vivareal.com.application.appApplication
import emerson.vivareal.com.data.database.AppDatabase
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.view.custom.status_null.model.StatusNullBookmarkModel
import emerson.vivareal.com.view.custom.status_null.model.StatusNullSearch
import emerson.vivareal.com.view.games.details.GameDetailActivity
import kotlinx.android.synthetic.main.fragment_bookmark_list_view.*
import javax.inject.Inject

class BookmarkListFragment : Fragment(), BookmarkListContract.View {
    @Inject
    lateinit var presenter: BookmarkListContract.Presenter

    @Inject
    lateinit var database: AppDatabase

    lateinit var adapter: BookmarkListAdapter

    lateinit var broadcastReceiver: BroadcastReceiver

    var querySearch: String = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (resultCode == Activity.RESULT_OK && intent != null) {
            when (requestCode) {
                DETAIL_CODE -> {
                    val modelGame: ModelGame = intent.getParcelableExtra("Model")

                    if (!modelGame.isBookmark) {
                        removeBookmark(modelGame.id, false)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bookmark_list_view, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.fragment_top_games, menu)

        updateItemLayoutIcon(menu?.findItem(R.id.item_layout))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.item_layout -> {
                adapter.gridLayout = !adapter.gridLayout

                updateItemLayoutIcon(item)

                recyclerLayoutView()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun updateItemLayoutIcon(item: MenuItem?) {
        if (adapter.gridLayout) {
            item?.icon = resources.getDrawable(R.drawable.ic_view_list_black_24dp)
        } else {
            item?.icon = resources.getDrawable(R.drawable.ic_view_module_black_24dp)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        appApplication.bookmarkListComponent.inject(this)

        initRecyclerView()

        initStatusNullBookmark()

        init()

        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val stringExtra = intent?.getStringExtra("query")

                querySearch = stringExtra ?: ""

                adapter.filter(querySearch)
            }
        }
    }

    override fun onPause() {
        super.onPause()

        presenter.pause()

        activity?.unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()

        presenter.setView(this)

        activity?.registerReceiver(broadcastReceiver, IntentFilter("search"))
    }

    private fun initStatusNullBookmark() {
        status_null_bookmark.binding?.observable = StatusNullBookmarkModel(activity)

        status_null_search.binding?.observable = StatusNullSearch(activity)
    }

    private fun initRecyclerView() {
        adapter = BookmarkListAdapter()
        adapter.presenter = presenter
        adapter.view = this

        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        recyclerLayoutView()
    }

    private fun init() {
        presenter.loadBookmarks()
    }

    private fun recyclerLayoutView() {
        recyclerView.layoutManager = if (adapter.gridLayout) {
            GridLayoutManager(activity, 2)
        } else {
            LinearLayoutManager(activity)
        }

        recyclerView.recycledViewPool.clear()

        adapter.notifyDataSetChanged()
    }

    override fun statusNullBookmark(statusNull: Boolean) {
        status_null_bookmark.visibility = if (querySearch.isBlank() && statusNull) View.VISIBLE else View.GONE
        status_null_search.visibility = if (querySearch.isNotEmpty() && statusNull) View.VISIBLE else View.GONE
    }

    override fun clearList() {
        adapter.clear()
    }

    override fun populate(list: List<ModelGame>) {
        adapter.setBookmark(list)
    }

    override fun openDetails(modelGame: ModelGame, views: List<View>) {
        GameDetailActivity.open(this, DETAIL_CODE, modelGame, views)
    }

    override fun removeBookmark(id: Int, showSnack: Boolean) {
        adapter.removeBookmark(id)

        if (showSnack) view?.let {
            Snackbar.make(it, getString(R.string.removed_bookmark), Snackbar.LENGTH_SHORT).show()
        }
    }

    companion object {
        val DETAIL_CODE = 1
    }
}
