package emerson.vivareal.com.view.games.list.games

import android.util.Log
import emerson.vivareal.com.data.api.TopGames.model.TopItem
import emerson.vivareal.com.data.database.bookmarks.BookmarkDao
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.view.games.list.GamesListService
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GamesListPresenter(
        private val service: GamesListService,
        private val bookmarkDao: BookmarkDao) :
        GamesListContract.Presenter {

    private val TAG = GamesListPresenter::class.java.simpleName
    var bookmarkList: MutableList<ModelGame> = mutableListOf()
    private var _view: GamesListContract.View? = null
    private var disposable = CompositeDisposable()
    private var page: Int = 0
    private var end = false

    override fun load(page: Int) {
        if (page > 0 && end) {
            return
        }

        _view?.loading(true)

        disposable.add(service.loadTopGames(page, LIMIT)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            this.page = page

                            if (page >= it.Total) {
                                end = true
                            }

                            onSuccessLoad(it.top)
                        },
                        {
                            _view?.showAPIError()
                            onErrorLoad(it)
                        }
                ))
    }

    override fun setView(view: GamesListContract.View) {
        this._view = view
    }

    override fun pause() {
        this._view = null
    }

    override fun loadMore() {
        load(page + LIMIT)
    }

    override fun onErrorLoad(throwable: Throwable) {
        _view?.setError(true)

        _view?.loading(false)
    }

    override fun onSuccessLoad(topGames: List<TopItem>?) {
        topGames?.let {
            if (page == 0) {
                _view?.clearList()
                end = false
            }

            val items: MutableList<ModelGame> = arrayListOf()

            it.forEach {
                val modelGame = ModelGame(it)

                bookmarkList.find { bookmark ->
                    bookmark.id == it.game.Id
                }?.let {
                    modelGame.isBookmark = true
                }

                items.add(modelGame)
            }

            _view?.populate(items)
        }

        _view?.loading(false)

        _view?.setError(false)
    }

    override fun loadBookmarks() {
        bookmarkDao.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    bookmarkList = it.toMutableList()
                })
    }

    override fun toggleBookmark(modelGame: ModelGame) {
        if (!modelGame.isBookmark) {
            Completable.fromAction { bookmarkDao.insert(modelGame) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                _view?.addBookmark(modelGame.id, true)
                            },
                            {
                                _view?.addBookmark(modelGame.id, true)
                            })
        } else {
            Completable.fromAction { bookmarkDao.delete(modelGame) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                _view?.removeBookmark(modelGame.id, true)
                            },
                            {
                                Log.e(TAG, "Remove bookmark error: ${it.message}")
                            })
        }
    }

    companion object {
        val LIMIT = 20
    }
}