package emerson.vivareal.com.view.games.list.bookmark

import android.util.Log
import emerson.vivareal.com.data.database.bookmarks.BookmarkDao
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class BookmarkListPresenter(private val bookmarkDao: BookmarkDao) :
        BookmarkListContract.Presenter {

    private val TAG = BookmarkListPresenter::class.java.simpleName
    private var _view: BookmarkListContract.View? = null
    var subscribe: Disposable? = null

    override fun setView(view: BookmarkListContract.View) {
        this._view = view
    }

    override fun pause() {
        this._view = null

        subscribe?.dispose()
    }

    override fun loadBookmarks() {
        subscribe = bookmarkDao.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it.forEach {
                        it.isBookmark = true
                    }

                    val sortedByDescending = it.sortedByDescending {
                        it.viewers
                    }

                    _view?.populate(sortedByDescending)

                    subscribe?.dispose()
                })
    }

    override fun removeBookmark(modelGame: ModelGame) {
        Completable.fromAction { bookmarkDao.delete(modelGame) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            _view?.removeBookmark(modelGame.id, true)
                        },
                        {
                            Log.e(TAG, "Remove bookmark error: ${it.message}")
                        })
    }
}