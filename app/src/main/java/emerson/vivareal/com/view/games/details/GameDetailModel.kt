package emerson.vivareal.com.view.games.details

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.drawable.Drawable
import android.view.View
import emerson.vivareal.com.BR
import emerson.vivareal.com.R
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.view.custom.frescoimage.ImageResUrls

class GameDetailModel(
        private val context: Context,
        private val modelGame: ModelGame)
    : BaseObservable() {

    var bookmarkListener: View.OnClickListener? = null
        @Bindable get

    fun getName(): String? {
        return modelGame.name
    }

    fun getViewers(): String {
        val views = modelGame.viewers

        return "$views ${context.getString(R.string.views)}"
    }

    fun getBox(): ImageResUrls {
        val lowResUrl = modelGame.box_small
        val highResUri = modelGame.box_larger

        return ImageResUrls(highResUri)
    }

    var bookmarkColor: Int = R.color.md_grey_600
        @Bindable get() {
            return if (modelGame.isBookmark) {
                R.color.colorAccent
            } else {
                R.color.md_grey_600
            }
        }

    var bookmarkIcon: Drawable? = null
        @Bindable get() {
            return if (modelGame.isBookmark) {
                context.resources.getDrawable(R.drawable.ic_bookmark_black_24dp)
            } else {
                context.resources.getDrawable(R.drawable.ic_bookmark_border_black_24dp)
            }
        }

    fun setBookmark(isBookmark: Boolean){
        modelGame.isBookmark = isBookmark

        notifyPropertyChanged(BR.bookmarkColor)
        notifyPropertyChanged(BR.bookmarkIcon)
    }

}