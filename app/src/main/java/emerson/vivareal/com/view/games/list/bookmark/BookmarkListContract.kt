package emerson.vivareal.com.view.games.list.bookmark

import emerson.vivareal.com.data.database.bookmarks.ModelGame

interface BookmarkListContract {
    interface View {
        fun populate(list: List<ModelGame>)
        fun clearList()
        fun removeBookmark(id: Int, showSnack: Boolean)
        fun openDetails(modelGame: ModelGame, views: List<android.view.View>)
        fun statusNullBookmark(statusNull: Boolean)
    }

    interface Presenter {
        fun setView(view: BookmarkListContract.View)
        fun loadBookmarks()
        fun removeBookmark(model: ModelGame)
        fun pause()
    }
}