package emerson.vivareal.com.view.custom.status_null.model

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import emerson.vivareal.com.R
import emerson.vivareal.com.view.custom.status_null.StatusNullContract

class StatusNullSearch(val context: Context?) : BaseObservable(), StatusNullContract.View {

    override var actionListener: View.OnClickListener? = null
        @Bindable get

    override fun showButton(): Int {
        return View.GONE
    }

    override fun getButtonText(): String? {
        return null
    }

    override fun getTitle(): String? {
        return context?.getString(R.string.search_no_items)
    }

    override fun getSubtitle(): String? {
        return context?.getString(R.string.search_no_description)
    }

    override fun getAnimation(): Int? {
        return R.raw.search_ask_loop
    }
}