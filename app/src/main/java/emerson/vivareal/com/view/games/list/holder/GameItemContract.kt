package emerson.vivareal.com.view.games.list.holder

import emerson.vivareal.com.data.database.bookmarks.ModelGame

interface GameItemContract {
    interface View {
        fun initData()
        fun setIsBookmark(isBookmark: Boolean)
        fun setup(model: ModelGame)
        fun toggleBookmark(toggleBookmark: () -> Unit)
        fun openDetails(openDetails: (ModelGame, List<android.view.View>) -> Unit)
    }
}