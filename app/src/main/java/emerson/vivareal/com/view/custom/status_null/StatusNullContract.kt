package emerson.vivareal.com.view.custom.status_null

interface StatusNullContract {
    interface View {
        var actionListener: android.view.View.OnClickListener?
        fun getTitle(): String?
        fun getSubtitle(): String?
        fun getAnimation(): Int?
        fun showButton(): Int
        fun getButtonText(): String?
    }

    interface Presenter {
        fun setObservable(statusNullContract: View)
    }
}