package emerson.vivareal.com.view.games.details

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.util.Pair
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import emerson.vivareal.com.R
import emerson.vivareal.com.application.appApplication
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.databinding.ActivityGameDetailBinding
import kotlinx.android.synthetic.main.activity_game_detail.*
import javax.inject.Inject

open class GameDetailActivity : AppCompatActivity(), GameDetailContract.View {

    @Inject
    lateinit var presenter: GameDetailContract.Presenter

    var modelGame: ModelGame? = null
    var binding: ActivityGameDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_game_detail)

        appApplication.gameDetailComponent.inject(this)

        setSupportActionBar(toolbar)

        supportActionBar?.let {
            it.setDisplayShowHomeEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }

        val extras = intent.extras

        modelGame = extras.getParcelable("Model")

        modelGame?.let {
            title = it.name
            setupObservable(it)
        } ?: run {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.setDetailView(this)
    }

    override fun onPause() {
        super.onPause()

        presenter.pause()
    }

    private fun setupObservable(modelGame: ModelGame) {
        val gameDetailObservable = GameDetailModel(this, modelGame)

        gameDetailObservable.bookmarkListener = View.OnClickListener {
            presenter.bookmark(modelGame)
        }

        binding?.observable = gameDetailObservable
    }

    override fun updateBookmark(isBookmark: Boolean) {
        binding?.observable?.setBookmark(isBookmark)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition();
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun finish() {
        val intent = Intent()
        intent.putExtra("Model", modelGame)

        setResult(Activity.RESULT_OK, intent)

        super.finish()
    }

    companion object {
        fun open(fragment: Fragment, code: Int, model: ModelGame, views: List<View>) {
            val bundle = Bundle()

            bundle.putParcelable("Model", model)

            val intent = Intent(fragment.activity, GameDetailActivity::class.java)
            intent.putExtras(bundle)

            val optionsCompat =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(fragment.activity!!,
                            Pair.create(views[0], "imgGame")
//                            Pair.create(views[1], "txtTitle"),
//                            Pair.create(views[2], "txtViewers")
                    )

            fragment.startActivityForResult(intent, code, optionsCompat.toBundle())
        }
    }
}
