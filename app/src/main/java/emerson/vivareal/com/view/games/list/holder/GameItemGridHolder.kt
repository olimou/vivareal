package emerson.vivareal.com.view.games.list.holder

import emerson.vivareal.com.databinding.AdapterGameItemGridBinding

class GameItemGridHolder(private val binding: AdapterGameItemGridBinding) :
        GameItemHolder(binding.root) {

    override fun initData() {
        binding.topItem = observable
    }

    override fun setIsBookmark(isBookmark: Boolean) {
        binding.topItem?.isBookmark = isBookmark
    }
}