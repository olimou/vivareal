package emerson.vivareal.com.view.games.list.games

import emerson.vivareal.com.data.api.TopGames.model.TopItem
import emerson.vivareal.com.data.database.bookmarks.ModelGame

interface GamesListContract {
    interface View {
        fun loading(show: Boolean)
        fun populate(list: List<ModelGame>)
        fun clearList()
        fun addBookmark(id: Int, showSnack: Boolean)
        fun removeBookmark(id: Int, showSnack: Boolean)
        fun openDetails(modelGame: ModelGame, views: List<android.view.View>)
        fun showAPIError()
        fun statusNullConnection(statusNull: Boolean)
        fun setError(error: Boolean)
    }

    interface Presenter {
        fun pause()
        fun load(page: Int)
        fun loadMore()
        fun setView(view: View)
        fun onSuccessLoad(topGames: List<TopItem>?)
        fun onErrorLoad(throwable: Throwable)
        fun loadBookmarks()
        fun toggleBookmark(modelGame: ModelGame)
    }
}