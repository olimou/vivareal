package emerson.vivareal.com.view.custom.status_null.model

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import emerson.vivareal.com.R
import emerson.vivareal.com.view.custom.status_null.StatusNullContract

class StatusNullBookmarkModel(val context: Context?) : BaseObservable(), StatusNullContract.View {

    override var actionListener: View.OnClickListener? = null
        @Bindable get

    override fun showButton(): Int {
        return View.GONE
    }

    override fun getButtonText(): String? {
        return ""
    }

    override fun getTitle(): String? {
        return context?.getString(R.string.no_bookmarks)
    }

    override fun getSubtitle(): String? {
        return context?.getString(R.string.yout_games_here)
    }

    override fun getAnimation(): Int? {
        return R.raw.empty_box
    }
}