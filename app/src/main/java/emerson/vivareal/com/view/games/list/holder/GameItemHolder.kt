package emerson.vivareal.com.view.games.list.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import emerson.vivareal.com.R
import emerson.vivareal.com.data.database.bookmarks.ModelGame

abstract class GameItemHolder(view: View)
    : RecyclerView.ViewHolder(view), GameItemContract.View {

    var observable: GameItemModel? = null

    override fun setup(model: ModelGame) {
        observable = GameItemModel(itemView.context, model)

        observable?.isBookmark = model.isBookmark

        initData()
    }

    override fun toggleBookmark(toggleBookmark: () -> Unit) {
        observable?.bookMarkListener = View.OnClickListener {
            toggleBookmark()
        }
    }

    override fun openDetails(openDetails: (ModelGame, List<View>) -> Unit) {
        itemView.setOnClickListener {
            observable?.model?.let {
                openDetails(it,
                        arrayListOf(itemView.findViewById(R.id.img_game))
                )
            }
        }
    }
}