package emerson.vivareal.com.view.games.details.di

import dagger.Module
import dagger.Provides
import emerson.vivareal.com.data.database.AppDatabase
import emerson.vivareal.com.view.games.details.GameDetailContract
import emerson.vivareal.com.view.games.details.GameDetailPresenter

@Module
open class GameDetailModule {

    @Provides
    open fun providesGameDetailPresenter(appDatabase: AppDatabase): GameDetailContract.Presenter {
        return GameDetailPresenter(appDatabase.bookmarkDao())
    }
}