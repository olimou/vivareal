package emerson.vivareal.com.view.games.list.bookmark.di

import dagger.Subcomponent
import emerson.vivareal.com.view.games.list.bookmark.BookmarkListFragment

@Subcomponent(modules = [BookmarkListModule::class])
interface BookmarkListComponent {
    fun inject(listFragment: BookmarkListFragment)
}