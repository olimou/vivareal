package emerson.vivareal.com.view.custom.status_null.model

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import emerson.vivareal.com.R
import emerson.vivareal.com.view.custom.status_null.StatusNullContract

class StatusNullConnectionModel(val context: Context?) : BaseObservable(), StatusNullContract.View {

    override var actionListener: View.OnClickListener? = null
        @Bindable get

    override fun showButton(): Int{
        return View.VISIBLE
    }

    override fun getButtonText(): String? {
        return context?.getString(R.string.retry)
    }

    override fun getTitle(): String? {
        return context?.getString(R.string.no_conection)
    }

    override fun getSubtitle(): String? {
        return context?.getString(R.string.check_connection)
    }

    override fun getAnimation(): Int? {
        return R.raw.cloud_disconnection
    }
}