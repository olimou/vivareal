package emerson.vivareal.com.view.custom.dialog_connection

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import emerson.vivareal.com.R

class DialogConnectionFragment : DialogFragment() {
    val FragmentTAG = "dialog_connection"

    var positiveListener: DialogInterface.OnClickListener? = null
    var cancelListener: DialogInterface.OnClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)
        builder.setView(R.layout.dialog_connection)
        builder.setPositiveButton(getString(R.string.retry), positiveListener)
        builder.setNegativeButton(getString(R.string.cancel), cancelListener)
        return builder.create()
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, FragmentTAG)
    }
}