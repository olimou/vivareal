package emerson.vivareal.com.view.games.list.bookmark

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import emerson.vivareal.com.databinding.AdapterGameItemGridBinding
import emerson.vivareal.com.databinding.AdapterGameItemListBinding
import emerson.vivareal.com.view.games.list.holder.GameItemGridHolder
import emerson.vivareal.com.view.games.list.holder.GameItemHolder
import emerson.vivareal.com.view.games.list.holder.GameItemListHolder

class BookmarkListAdapter :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = BookmarkListAdapter::class.java.simpleName
    private var listView: MutableSet<ModelGame> = mutableSetOf()
    private var listOriginal: MutableSet<ModelGame> = mutableSetOf()

    var presenter: BookmarkListContract.Presenter? = null
    var view: BookmarkListContract.View? = null
    var gridLayout = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return if (gridLayout) {
            GameItemGridHolder(AdapterGameItemGridBinding.inflate(layoutInflater, parent, false))
        } else {
            GameItemListHolder(AdapterGameItemListBinding.inflate(layoutInflater, parent, false))
        }
    }

    override fun getItemCount(): Int {
        val size = listView.size

        view?.statusNullBookmark(size == 0)

        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = listView.elementAt(position)

        val gameItemHolder = holder as GameItemHolder

        gameItemHolder.setup(model)

        gameItemHolder.toggleBookmark {
            presenter?.removeBookmark(model)
        }

        gameItemHolder.openDetails { modelGame, views ->
            view?.openDetails(modelGame, views)
        }
    }

    fun removeBookmark(id: Int?) {
        listView.find { it.id == id }?.let {
            val indexOf = listView.indexOf(it)

            listView.remove(it)

            notifyItemRemoved(indexOf)
        }
    }

    fun clear() {
        listView.clear()

        notifyDataSetChanged()
    }

    fun setBookmark(list: List<ModelGame>) {
        listView = list.toMutableSet()
        listOriginal = listView

        notifyDataSetChanged()
    }

    fun filter(stringExtra: String) {
        listView = listOriginal

        if (stringExtra.isNotEmpty()) {
            val query = stringExtra.toLowerCase()

            val filter: List<ModelGame> = listView.filter {
                it.name?.toLowerCase()?.contains(query) ?: false
            }

            listView = filter.toMutableSet()
        }

        notifyDataSetChanged()
    }
}