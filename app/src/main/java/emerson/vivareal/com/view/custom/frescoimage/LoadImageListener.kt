package emerson.vivareal.com.view.custom.frescoimage

import android.graphics.Bitmap

interface LoadImageListener {
    fun onFinish(bitmap: Bitmap)
}