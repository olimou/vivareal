package emerson.vivareal.com.view.games.list.bookmark.di

import dagger.Module
import dagger.Provides
import emerson.vivareal.com.data.database.AppDatabase
import emerson.vivareal.com.view.games.list.bookmark.BookmarkListContract
import emerson.vivareal.com.view.games.list.bookmark.BookmarkListPresenter

@Module
class BookmarkListModule {

    @Provides
    fun provideGamesPresenter(appDatabase: AppDatabase): BookmarkListContract.Presenter {
        return BookmarkListPresenter(appDatabase.bookmarkDao())
    }
}