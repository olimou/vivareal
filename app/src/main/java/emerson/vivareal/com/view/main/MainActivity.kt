package emerson.vivareal.com.view.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import emerson.vivareal.com.R
import emerson.vivareal.com.view.games.list.bookmark.BookmarkListFragment
import emerson.vivareal.com.view.games.list.games.GamesListFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    var mActionView: MenuItem? = null
    var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        initBottomNavigation()

        init()
    }

    private fun init() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment, GamesListFragment()).commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)

        //Pega o Componente.
        mActionView = menu.findItem(R.id.search)

        searchView = mActionView?.actionView as SearchView

        //Define um texto de ajuda:
        searchView?.queryHint = "Busca"

        // exemplos de utilização:
        searchView?.setOnQueryTextListener(this)

        searchView?.setOnCloseListener {
            sendSearch("")
            true
        }

        return true
    }

    private fun sendSearch(query: String?) {
        val intent = Intent()
        intent.action = "search"
        intent.putExtra("query", query)
        sendBroadcast(intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation service item clicks here.
        when (item.itemId) {
            R.id.nav_top_games -> {
                openGameList()
            }
            R.id.nav_bookmark -> {
                openBookmark()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)

        return true
    }

    private fun initBottomNavigation() {
        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_top_games -> {
                    openGameList()
                    true
                }
                R.id.action_favorites -> {
                    openBookmark()
                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun openBookmark() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment, BookmarkListFragment()).commit()
    }

    private fun openGameList() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment, GamesListFragment()).commit()
    }

    val TAG = MainActivity::class.java.simpleName

    override fun onQueryTextSubmit(query: String?): Boolean {
        Log.d(TAG, "querySubmit: $query")

        hideKeyboard()

        return true
    }

    private fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        Log.d(TAG, "quertChange: $newText")

        sendSearch(newText)

        return true
    }
}
