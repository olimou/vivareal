package emerson.vivareal.com.view.games.details

import android.util.Log
import emerson.vivareal.com.data.database.bookmarks.BookmarkDao
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class GameDetailPresenter(private val bookmarkDao: BookmarkDao) : GameDetailContract.Presenter {
    var TAG = GameDetailPresenter::class.java.simpleName

    var view: GameDetailContract.View? = null

    override fun setDetailView(view: GameDetailContract.View) {
        this.view = view
    }

    override fun pause() {
        this.view = null
    }

    override fun bookmark(modelGame: ModelGame) {
        if (modelGame.isBookmark) {
            Completable.fromAction({ bookmarkDao.delete(modelGame) })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                view?.updateBookmark(false)
                            },
                            {
                                Log.e(TAG, "Error on delete item: ${it.message}")
                            }
                    )
        } else {
            Completable.fromAction({ bookmarkDao.insert(modelGame) })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                view?.updateBookmark(true)
                            },
                            {
                                Log.e(TAG, "Error on insert item: ${it.message}")
                            }
                    )
        }
    }
}