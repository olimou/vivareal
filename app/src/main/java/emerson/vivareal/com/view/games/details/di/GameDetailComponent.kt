package emerson.vivareal.com.view.games.details.di

import dagger.Subcomponent
import emerson.vivareal.com.view.games.details.GameDetailActivity

@Subcomponent(modules = [GameDetailModule::class])
interface GameDetailComponent {
    fun inject(gameDetailFragment: GameDetailActivity)
}