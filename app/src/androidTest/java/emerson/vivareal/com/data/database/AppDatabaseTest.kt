package emerson.vivareal.com.data.database

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import emerson.vivareal.com.data.database.bookmarks.BookmarkDao
import emerson.vivareal.com.data.database.bookmarks.ModelGame
import io.reactivex.subscribers.TestSubscriber
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest {
    lateinit var bookmarkDao: BookmarkDao
    lateinit var appDatabase: AppDatabase
    lateinit var modelGame: ModelGame
    lateinit var modelGame2: ModelGame
    lateinit var modelGame3: ModelGame

    @Before
    fun createDB() {
        val context = InstrumentationRegistry.getTargetContext()

        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).allowMainThreadQueries().build()

        bookmarkDao = appDatabase.bookmarkDao()

        modelGame = ModelGame()
        modelGame.id = 12

        modelGame2 = ModelGame()
        modelGame2.id = 13

        modelGame3 = ModelGame()
        modelGame3.id = 15
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        appDatabase.close()
    }

    @Test
    @Throws(IOException::class)
    fun createBookmarkGame() {
        bookmarkDao.insert(modelGame)
    }

    @Test
    @Throws(IOException::class)
    fun loadById() {
        bookmarkDao.insert(modelGame)

        val testSubscriber = TestSubscriber<ModelGame>()

        bookmarkDao.loadById(12).subscribe(testSubscriber)

        testSubscriber.awaitCount(1)

        testSubscriber.assertValue {
            it.id != 13
        }

        testSubscriber.assertValue {
            it.id == 12
        }
    }

    @Test
    @Throws(IOException::class)
    fun getAll() {
        bookmarkDao.insert(modelGame)
        bookmarkDao.insert(modelGame2)
        bookmarkDao.insert(modelGame3)

        val subscriber = TestSubscriber<List<ModelGame>>()

        bookmarkDao.getAll().subscribe(subscriber)

        subscriber.awaitCount(1)

        subscriber.assertValue {
            it.size == 3
        }
    }

    @Test
    @Throws(IOException::class)
    fun delete() {
        bookmarkDao.insert(modelGame)
        bookmarkDao.insert(modelGame2)
        bookmarkDao.insert(modelGame3)

        bookmarkDao.delete(modelGame2)

        val subscriber = TestSubscriber<List<ModelGame>>()

        bookmarkDao.getAll().subscribe(subscriber)

        subscriber.awaitCount(1)

        subscriber.assertValue {
            it.size == 2
        }

        subscriber.assertValue {
            !it.contains(modelGame2)
        }
    }
}
